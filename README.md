# EEE3088F_JNRMAT002_EYSMUH002_ASMABU002
The aim of this project is to develop, as a three person group, a Motor Driver PiHAT. This PiHAT is deisned to be an attachable circuit that allows the control of a DC Motor in two directions. Additionally, there will be lighting to indicate which direction the motor is turning or whether it is stationary.

How to use:

Logic inputs IN1 and IN2 control which direction the motor is turning. The driver itself is funded by a 9V external voltage source. In order to use the logic inputs, use the pins on the Pi Zero itself - can be buttons/switches or programmed/automated.
