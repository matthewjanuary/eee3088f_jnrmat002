The number of layers the PCB is designed with in KiCAD is four.
The complexity of the design is not very extreme - only a handful of components are used/needed in order for the PCB to constructed and operated.

The physical dimensions of the board are detailed in the KiCAD PCB file itself.

In addition, hole dimensions and wiring diameters were chosen to be slightly thicker (around 3mm) in order for easier soldering.
The PCB edges are rounded and components are fairly close to the edge; in addition, board coverage using the parts available is quite complete.

In addition to the L293D IC having heat sinking grounds built-in, parts connected to ground are grounded through multiple spots to avoid build-ups of inductance and high current (through vias).

All parts selected were surface-mount components and not through-hole components.

In order to manufacture yourself, you will have to generate Gerber Files through KiCAD within the KiCAD files stored in the git repo.
Included in these Gerber Files, you will need all copper layers, silkscreen layers, vias and other necessary components.