40-pin GPIO Connector - Template used on gitlab: https://gitlab.com/matthewjanuary/eee3088f_jnrmat002.git

For LED Circuit: KiCAD Schematic on gitlab (in StatusLEDs Folder): https://gitlab.com/matthewjanuary/eee3088f_jnrmat002.git
 -Two LEDs
 -1k Ohm Resistor

For Hall Effect Sensor: KiCAD Schematic on gitlab (in Hall Sensor Folder): https://gitlab.com/matthewjanuary/eee3088f_jnrmat002.git
 -DRV5013
 ~used this component as it still fit the specifications required for out PiHAT~

For Power Supply Circuit: KiCAD Schematic on gitlab (in Power Supply Folder): https://gitlab.com/matthewjanuary/eee3088f_jnrmat002.git
 -3.3V inputs (from GPIO Connector pins)
 -0.1uF Decoupling Capacitor
 -5V input (from GPIO Connector pin)
 -L293D Motor Driver IC
 -9V input (from external power source)
 -2 1k Ohm Resistors
 -1 DC Motor