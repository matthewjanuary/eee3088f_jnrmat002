Connect 3.3V GPIO pins as the input power to the L293D IC - one to the 1A input and the other to the 2A input.
Connect 5V GPIO pin as Vcc power (pin 16 to the L293D IC.
Connect a 9V battery/external voltage source as the second Vcc (pin 8) for the L293D - this is the motor's power.

In order to move the motor in the forward position, set the GPIO 3.3V pin output (at pin 2 on L293D) to HIGH (second GPIO 3.3V pin output to LOW).
In order to move the motor in the reverse position, set the GPIO 3.3V pin output (at pin 7 on L293D) to HIGH (first GPIO 3.3V pin output to LOW).

Any other state for the GPIO pins (i.e. - both HIGH or both LOW) will result in the motor being stationary.