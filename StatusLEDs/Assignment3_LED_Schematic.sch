EESchema Schematic File Version 4
LIBS:practical 1-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:LED D1
U 1 1 60789278
P 3850 1650
F 0 "D1" H 3843 1866 50  0000 C CNN
F 1 "LED" H 3843 1775 50  0000 C CNN
F 2 "" H 3850 1650 50  0001 C CNN
F 3 "~" H 3850 1650 50  0001 C CNN
	1    3850 1650
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D2
U 1 1 60789D0A
P 3850 2150
F 0 "D2" H 3843 1895 50  0000 C CNN
F 1 "LED" H 3843 1986 50  0000 C CNN
F 2 "" H 3850 2150 50  0001 C CNN
F 3 "~" H 3850 2150 50  0001 C CNN
	1    3850 2150
	-1   0    0    1   
$EndComp
Wire Wire Line
	4000 1650 4350 1650
Wire Wire Line
	4350 1650 4350 1900
Wire Wire Line
	4350 2150 4000 2150
Wire Wire Line
	3700 1650 3350 1650
Wire Wire Line
	3350 1650 3350 1900
Wire Wire Line
	3350 2150 3700 2150
Wire Wire Line
	3350 1900 3000 1900
Connection ~ 3350 1900
Wire Wire Line
	3350 1900 3350 2150
Wire Wire Line
	4350 1900 4500 1900
Connection ~ 4350 1900
Wire Wire Line
	4350 1900 4350 2150
$Comp
L Device:R_Small_US R1
U 1 1 6078EA0C
P 2900 1900
F 0 "R1" V 2695 1900 50  0000 C CNN
F 1 "1k" V 2786 1900 50  0000 C CNN
F 2 "" H 2900 1900 50  0001 C CNN
F 3 "~" H 2900 1900 50  0001 C CNN
	1    2900 1900
	0    1    1    0   
$EndComp
Wire Wire Line
	4500 1900 4500 2350
Wire Wire Line
	4500 2350 2100 2350
Connection ~ 4500 1900
Wire Wire Line
	4500 1900 4700 1900
Text Label 2100 1900 2    50   ~ 0
L293D_Output_1
Text Label 2100 2350 2    50   ~ 0
L293D_Output_2
Wire Wire Line
	2100 1900 2500 1900
Wire Wire Line
	2500 1900 2500 1400
Wire Wire Line
	2500 1400 4700 1400
Connection ~ 2500 1900
Wire Wire Line
	2500 1900 2800 1900
$Comp
L Motor:Motor_DC M1
U 1 1 6079A3D9
P 4700 1600
F 0 "M1" H 4858 1596 50  0000 L CNN
F 1 "Motor_DC" H 4858 1505 50  0000 L CNN
F 2 "" H 4700 1510 50  0001 C CNN
F 3 "~" H 4700 1510 50  0001 C CNN
	1    4700 1600
	1    0    0    -1  
$EndComp
$EndSCHEMATC
