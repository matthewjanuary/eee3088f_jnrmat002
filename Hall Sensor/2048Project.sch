EESchema Schematic File Version 4
LIBS:2048Project-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Device:C_Small C1
U 1 1 6078255D
P 3350 2150
F 0 "C1" V 3121 2150 50  0000 C CNN
F 1 "0.01u" V 3212 2150 50  0000 C CNN
F 2 "" H 3350 2150 50  0001 C CNN
F 3 "~" H 3350 2150 50  0001 C CNN
	1    3350 2150
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR?
U 1 1 60782FA9
P 3600 2550
F 0 "#PWR?" H 3600 2300 50  0001 C CNN
F 1 "GND" H 3605 2377 50  0000 C CNN
F 2 "" H 3600 2550 50  0001 C CNN
F 3 "" H 3600 2550 50  0001 C CNN
	1    3600 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	3450 2150 3600 2150
Wire Wire Line
	3600 2150 3600 2550
$Comp
L Sensor_Current:A1363xKTTN-1 U?
U 1 1 60783AA1
P 2800 3300
F 0 "U?" H 2570 3346 50  0000 R CNN
F 1 "A1363xKTTN-1" H 2570 3255 50  0000 R CNN
F 2 "Sensor_Current:Allegro_SIP-4" H 3150 3200 50  0001 L CIN
F 3 "http://www.allegromicro.com/~/media/Files/Datasheets/A1363-Datasheet.ashx?la=en" H 2800 3300 50  0001 C CNN
	1    2800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3250 2150 2800 2150
Wire Wire Line
	2800 2150 2800 3000
$Comp
L Device:R_Small_US R1
U 1 1 607887A1
P 3250 1200
F 0 "R1" V 3045 1200 50  0000 C CNN
F 1 "1000" V 3136 1200 50  0000 C CNN
F 2 "" H 3250 1200 50  0001 C CNN
F 3 "~" H 3250 1200 50  0001 C CNN
	1    3250 1200
	0    1    1    0   
$EndComp
Wire Wire Line
	2800 2150 2800 1200
Wire Wire Line
	2800 1200 3150 1200
Connection ~ 2800 2150
$Comp
L Device:R_Small_US R2
U 1 1 60789247
P 4450 1950
F 0 "R2" H 4518 1996 50  0000 L CNN
F 1 "10k" H 4518 1905 50  0000 L CNN
F 2 "" H 4450 1950 50  0001 C CNN
F 3 "~" H 4450 1950 50  0001 C CNN
	1    4450 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R3
U 1 1 60789D84
P 5500 1950
F 0 "R3" H 5568 1996 50  0000 L CNN
F 1 "3.3k" H 5568 1905 50  0000 L CNN
F 2 "" H 5500 1950 50  0001 C CNN
F 3 "~" H 5500 1950 50  0001 C CNN
	1    5500 1950
	1    0    0    -1  
$EndComp
$Comp
L Device:R_Small_US R4
U 1 1 6078A0D0
P 6250 1400
F 0 "R4" H 6318 1446 50  0000 L CNN
F 1 "470" H 6318 1355 50  0000 L CNN
F 2 "" H 6250 1400 50  0001 C CNN
F 3 "~" H 6250 1400 50  0001 C CNN
	1    6250 1400
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D?
U 1 1 6078A7A7
P 6250 1900
F 0 "D?" V 6289 1783 50  0000 R CNN
F 1 "LED" V 6198 1783 50  0000 R CNN
F 2 "" H 6250 1900 50  0001 C CNN
F 3 "~" H 6250 1900 50  0001 C CNN
	1    6250 1900
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3350 1200 4450 1200
Wire Wire Line
	4450 1200 4450 1850
Wire Wire Line
	4450 1200 5500 1200
Wire Wire Line
	5500 1200 5500 1850
Connection ~ 4450 1200
Wire Wire Line
	5500 1200 6250 1200
Wire Wire Line
	6250 1200 6250 1300
Connection ~ 5500 1200
Wire Wire Line
	6250 1500 6250 1750
$Comp
L Device:Q_NPN_BCE Q?
U 1 1 6078BCEE
P 4700 4000
F 0 "Q?" H 4891 4046 50  0000 L CNN
F 1 "Q_NPN_BCE" H 4891 3955 50  0000 L CNN
F 2 "" H 4900 4100 50  0001 C CNN
F 3 "~" H 4700 4000 50  0001 C CNN
	1    4700 4000
	1    0    0    -1  
$EndComp
$Comp
L Device:Q_NPN_BCE Q?
U 1 1 6078C394
P 7250 3300
F 0 "Q?" H 7441 3346 50  0000 L CNN
F 1 "Q_NPN_BCE" H 7441 3255 50  0000 L CNN
F 2 "" H 7450 3400 50  0001 C CNN
F 3 "~" H 7250 3300 50  0001 C CNN
	1    7250 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4450 2050 4450 3300
Wire Wire Line
	4450 3300 3200 3300
Wire Wire Line
	5500 2050 5500 2250
Wire Wire Line
	5500 3300 7050 3300
Wire Wire Line
	4450 3300 4450 4000
Wire Wire Line
	4450 4000 4500 4000
Connection ~ 4450 3300
Wire Wire Line
	4800 3800 4800 2250
Wire Wire Line
	4800 2250 5500 2250
Connection ~ 5500 2250
Wire Wire Line
	5500 2250 5500 3300
Wire Wire Line
	6250 2050 6250 2550
Wire Wire Line
	6250 2550 6800 2550
Wire Wire Line
	7350 3100 7350 2800
Wire Wire Line
	7350 2800 6800 2800
Wire Wire Line
	6800 2800 6800 2550
Connection ~ 6800 2550
Wire Wire Line
	6800 2550 6950 2550
$Comp
L Device:Battery BT?
U 1 1 6079299F
P 9100 3550
F 0 "BT?" H 9208 3596 50  0000 L CNN
F 1 "Battery" H 9208 3505 50  0000 L CNN
F 2 "" V 9100 3610 50  0001 C CNN
F 3 "~" V 9100 3610 50  0001 C CNN
	1    9100 3550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6250 1200 7350 1200
Wire Wire Line
	9100 1200 9100 3350
Connection ~ 6250 1200
Wire Wire Line
	7350 1750 7350 1450
Connection ~ 7350 1200
Wire Wire Line
	7350 1200 9100 1200
$Comp
L power:GND #PWR?
U 1 1 607A1933
P 5850 5050
F 0 "#PWR?" H 5850 4800 50  0001 C CNN
F 1 "GND" H 5855 4877 50  0000 C CNN
F 2 "" H 5850 5050 50  0001 C CNN
F 3 "" H 5850 5050 50  0001 C CNN
	1    5850 5050
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 3750 9100 5050
Wire Wire Line
	9100 5050 7350 5050
Wire Wire Line
	2800 3600 2800 5050
Wire Wire Line
	2800 5050 4800 5050
Connection ~ 5850 5050
Wire Wire Line
	4800 4200 4800 5050
Connection ~ 4800 5050
Wire Wire Line
	4800 5050 5850 5050
Wire Wire Line
	7350 3500 7350 5050
Connection ~ 7350 5050
Wire Wire Line
	7350 5050 5850 5050
$Comp
L Device:R_Small_US R5
U 1 1 60B7E825
P 7350 1850
F 0 "R5" H 7418 1896 50  0000 L CNN
F 1 "10k" H 7418 1805 50  0000 L CNN
F 2 "" H 7350 1850 50  0001 C CNN
F 3 "~" H 7350 1850 50  0001 C CNN
	1    7350 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7350 1950 7350 2550
$Comp
L Simulation_SPICE:DIODE D?
U 1 1 60B7F6CC
P 6950 1750
F 0 "D?" V 6996 1670 50  0000 R CNN
F 1 "DIODE" V 6905 1670 50  0000 R CNN
F 2 "" H 6950 1750 50  0001 C CNN
F 3 "~" H 6950 1750 50  0001 C CNN
F 4 "Y" H 6950 1750 50  0001 L CNN "Spice_Netlist_Enabled"
F 5 "V" H 6950 1750 50  0001 L CNN "Spice_Primitive"
	1    6950 1750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6950 1900 6950 2550
Connection ~ 6950 2550
Wire Wire Line
	6950 2550 7350 2550
Wire Wire Line
	6950 1600 6950 1450
Wire Wire Line
	6950 1450 7350 1450
Connection ~ 7350 1450
Wire Wire Line
	7350 1450 7350 1200
Text Label 8200 7650 0    50   ~ 0
03-06-2021
Text Label 8300 7500 0    50   Italic 0
Amplifier_circuit_V0.2
Text Label 10700 7650 0    50   Italic 0
0.2
$EndSCHEMATC
